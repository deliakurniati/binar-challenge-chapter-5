$(document).ready(function () {
	$.ajax({
		method: 'GET',
		type: 'JSON',
    url: 'http://localhost:3000/api/data',
    beforeSend: function () {
      $('#loading').show();
    },
		success: function(data) {
			
			console.log('getData', data);
			let stringHtmlPlayer = '';
			let stringHtmlComputer = '';

			$.each(data, function (idx, obj) {                                   
				stringHtmlPlayer += '<button class="button" type="button" style="cursor: pointer;" value="'+obj.name.toLowerCase()+'">'
				stringHtmlPlayer += 		'<img src="'+obj.image +'" value="'+obj.name.toLowerCase()+'" style="max-width: 100px;">'
				stringHtmlPlayer += '</button>'

				stringHtmlComputer += '<button class="button-com" type="button" style="pointer-events: none;" value="'+idx+'">'
				stringHtmlComputer += 		'<img src="'+obj.image +'" value="'+obj.name.toLowerCase()+'" style="max-width: 100px;">'
				stringHtmlComputer += '</button>'
			});

			$("#section-img-player").html(stringHtmlPlayer);
			$("#section-img-computer").html(stringHtmlComputer);

			let scorePlayer = document.querySelector('.score-player');
			let scoreComputer = document.querySelector('.score-computer');
			let message = document.querySelector('.message');
			let buttons = document.querySelectorAll('.button');
			let buttonsCom = document.querySelectorAll('.button-com');
			let winner = [0, 0];
			let computerOptions = ["rock", "scissors", "paper"];

			for (let i = 0; i < buttons.length; i++) {
				buttons[i].addEventListener('click', startGame)
			}

			function startGame(e) {
				let previousElement = document.querySelector('.button-choose');

				if (previousElement) {
					previousElement.classList.remove('button-choose');
				}

        (this.classList.length <= 2) ? 
          this.classList.add("button-choose") : this.classList.remove("button-choose");

				let playerSelects   = e.target.getAttribute('value');
        let computerSelects = Math.floor(Math.random() * computerOptions.length);

				for (let i = 0; i < buttonsCom.length; i++) {
          (buttonsCom[i].value == computerSelects) ?
            buttonsCom[i].classList.add('button-choose-com') 
            : buttonsCom[i].classList.remove('button-choose-com');
				}

				let result = whoIsTheWinner(playerSelects, computerOptions[computerSelects]);

				if (result === "player") {
					winner[0]++;
					result += ' 1 Wins!'
				}

				if (result === "computer") {
					winner[1]++;
					result += ' Wins!'
				}

				if (result === "draw") { result = 'draw' }

				scorePlayer.innerHTML = winner[0]
				scoreComputer.innerHTML = winner[1]
				setMessage(result);
			}

			function setMessage(selectionMessage) {
				message.style.transform = 'rotate(-30.98deg)';
				message.style.background = 'green';
				message.style.fontSize = '28px';
				message.style.color = 'white';
				message.style.paddingTop = '10px';
				message.style.paddingBottom = '20px';
				message.innerHTML = selectionMessage
			}

			function whoIsTheWinner(player, computer) {
				if (player === computer) {
          return 'draw'
				}
				if (player === "rock") {
          return (computer === "scissors") ? 'player' : 'computer';
				}
				if (player === "scissors") {
          return (computer === "paper") ? 'player' : 'computer';
				}
				if (player === "paper") {
          return (computer === "rock") ? 'player' : 'computer';
				}
			}
    },
    complete: function () {
      setTimeout(() => {
        $('#loading').fadeOut(1000)
      }, 1500);
    },
 	});
});