const express = require('express')
const router  = express.Router()
const data    = require('../Database/data.json')

router.get('/api/data', function (req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  res.status(200).json(data)
})

module.exports = router