const express    = require('express')
const app 		 = express()
const port 	 	 = 3000
const bodyParser = require('body-parser');

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static('public'));

const router = require('./routing')
app.use(router)

/** Internal Server Error Handler */

/** 505 Handler */
app.use(function (err, req, res, next) {
	console.error(err);
	res.status(500).json({
		status: 500,
		errors: 'Something when wrong'
	})
})

/** 404 Handler */
app.use(function (err, req, res, next) {
	res.status(404).json({
		status: 404,
		errors: 'Page not found'
	})
})

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))